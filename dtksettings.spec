Name:		dtksettings
Version:	0.1.6
Release:	1%{?dist}
Summary:	Deepin Tool Kit Settings library

Group:		System Environment/Libraries
License:	GPL-3+
URL:		http://deepin.org/dtk
Source0:	dtksettings_%{version}.orig.tar.xz

BuildRequires:	pkgconfig
BuildRequires:	qt5-qtbase-devel
#Requires:	

%description
Deepin Tool Kit(DTK) Settings library

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for dtksettings

%prep
%setup -q

%build
qmake-qt5  LIB_INSTALL_DIR=/usr/lib64
make %{?_smp_mflags} 

%install
make install INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}/usr/bin/dtk-settings-tool

%files
%defattr(-,root,root,-)
%doc
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%changelog

