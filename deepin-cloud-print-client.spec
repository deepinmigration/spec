Name:		deepin-cloud-print-client
Version:	1.0.1
Release:	1%{?dist}
Summary:	printer driver for Deepin Cloud Print writing via CUPS

Group:		System Environment
License:	Unknown
URL:		http://deepin.org/deepin-cloud-print-client
Source0:	deepin-cloud-print-client_%{version}.orig.tar.xz

BuildRequires:	pkgconfig
BuildRequires:	dtksettings-devel
BuildRequires:	deepin-tool-kit-devel
BuildRequires:	cups-devel
BuildRequires:	thrift-devel
BuildRequires:	qt5-qttools-devel
BuildRequires:	dbus-c++-devel
Requires:	cups
Requires:	deepin-splash
Requires:	qt5-qtsvg

%description
Printer driver for Deepin Cloud Print writing via CUPS

%prep
%setup -q

%build
qmake-qt5 
make %{?_smp_mflags} 

%install
make install INSTALL_ROOT=%{buildroot} PREFIX=/usr 

%files
%defattr(-,root,root,-)
%{_sysconfdir}/dbus-1/system.d/*
%{_sysconfdir}/xdg/autostart/*
%{_bindir}/*
%{_prefix}/lib/deepin-daemon/*
%{_prefix}/lib/cups/backend/*
%{_datadir}
%exclude %{_prefix}/lib/debug


%post
# Remove lint from possible upgrades.
if [ -f /etc/cups/cups-files.conf ];then
    sed -i 's|^SystemGroup.*$|SystemGroup sys root wheel|g' /etc/cups/cups-files.conf
fi
rm -rf /var/spool/dcp/SPOOL
# Sanitize ownerships and permissions.
chmod 0700 /usr/lib/cups/backend/dcp
if ! [ -f /var/log/cups/dcp_log ]
then
    touch /var/log/cups/dcp_log
fi
chmod 0640 /var/log/cups/dcp_log*
chown root:lpadmin /var/spool/dcp/ANONYMOUS
chmod 1777 /var/spool/dcp/ANONYMOUS
# Wait until CUPS has reloaded its configuration.
if lpstat -h localhost -r 2>/dev/null | grep -q not
then
    t=0
    while lpstat -h localhost -r 2>/dev/null | grep -q not
    do
            t=$(($t + 1))
        if [ $t = 10 ]
        then
                echo "CUPS failed to reload its configuration!"
            break
        fi
        sleep 1
    done
fi

%changelog
