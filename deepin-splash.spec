Name:		deepin-splash
Version:	1.0.0
Release:	1%{?dist}
Summary:	Deepin Splash

Group:		System Environment/Libraries
License:	Unknown
URL:		http://deepin.org/dtk
Source0:	deepin-splash_%{version}.orig.tar.xz

BuildRequires:	pkgconfig
BuildRequires:	openssl-devel
BuildRequires:	qt5-qtbase-devel
BuildRequires:	qt5-qttools-devel

%description
Deepin Splash for deepin-cloud-printer

%prep
%setup -q

%build
qmake-qt5  LIB_INSTALL_DIR=/usr/lib64
make %{?_smp_mflags} 

%install
make install INSTALL_ROOT=%{buildroot}

%files
%defattr(-,root,root,-)
%{_prefix}/lib/deepin-daemon/deepin-splash
%{_sysconfdir}/xdg/autostart/*
%{_sysconfdir}/dbus-1/system.d/*

%changelog

