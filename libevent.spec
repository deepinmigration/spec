Summary: Abstract asynchronous event notification library
Name: libevent
Version: 2.0.21
Release: 9%{?dist}
License: BSD
Group: System Environment/Libraries
URL: http://monkey.org/~provos/libevent/

Source: http://monkey.org/~provos/libevent_%{version}-stable.orig.tar.gz

BuildRequires: gcc-c++

%description
The libevent API provides a mechanism to execute a callback function
when a specific event occurs on a file descriptor or after a timeout
has been reached. libevent is meant to replace the asynchronous event
loop found in event driven network servers. An application just needs
to call event_dispatch() and can then add or remove events dynamically
without having to change the event loop.

%package devel
Summary: Header files, libraries and development documentation for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%prep
%setup

%build
%configure --disable-static
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR="%{buildroot}"
find %{buildroot} -name "*.la" -delete

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc README
%{_bindir}/event_rpcgen.py*
%{_libdir}/libevent*.so.*

%files devel
%defattr(-, root, root, 0755)
%doc sample/
%{_includedir}/*.h
%{_includedir}/event2/*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%exclude %{_libdir}/libevent.la

%changelog
* Mon Mar 05 2007 Dag Wieers <dag@wieers.com> - 1.3b-1
- Updated to release 1.3b.
