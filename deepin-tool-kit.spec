Name:		deepin-tool-kit
Version:	0.2.9
Release:	1%{?dist}
Summary:	Deepin Tool Kit library

Group:		System Environment/Libraries
License:	GPL-3+
URL:		http://deepin.org/dtk
Source0:	deepin-tool-kit_%{version}.orig.tar.xz

BuildRequires:	pkgconfig
BuildRequires:	dtksettings-devel
BuildRequires:	qt5-qtbase-devel
BuildRequires:	qt5-qtmultimedia-devel
BuildRequires:	qt5-qtx11extras-devel
BuildRequires:	qt5-qttools-devel
BuildRequires:	startup-notification-devel
BuildRequires:	fontconfig-devel
#Requires:	

%description
Deepin Tool Kit(DTK) library

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for dtk

%prep
%setup -q
sed -i 's|lrelease|lrelease-qt5|g' tool/translate_generation.sh

%build
qmake-qt5  LIB_INSTALL_DIR=/usr/lib64
make %{?_smp_mflags} 

%install
make install INSTALL_ROOT=%{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*
%{_datadir}/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/qt5/tests/*
%{_includedir}/*

%changelog

