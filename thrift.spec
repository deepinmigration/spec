Name:		thrift
Version:	0.9.2
Release:	1%{?dist}
Summary:	Thrift is a software framework for scalable cross-language services development.

Group:		System Environment/Libraries
License:	Apache
URL:		http://thrift.apache.org/
Source0:	thrift_%{version}.orig.tar.xz

BuildRequires:	pkgconfig
BuildRequires:	libevent-devel >= 2.0.21
BuildRequires:	boost-devel
BuildRequires:	bison
BuildRequires:	flex
#Requires:	

%description
Thrift is a software framework for scalable cross-language services.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for developing applications that use %{name}.

%package compiler
Summary: compiler for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description compiler
The %{name}-compiler package contains compiler tools for thrift definition files

%prep
%setup -q
./bootstrap.sh


%build
%configure --without-java --without-php --without-lua --without-python --without-test
make %{?_smp_mflags} || true
make %{?_smp_mflags} -C lib/cpp/


%install
make install DESTDIR=%{buildroot}
make -C lib/cpp/ install DESTDIR=%{buildroot}
find %{buildroot} -name "*.a" -delete
find %{buildroot} -name "*.la" -delete


%files
%defattr(-,root,root,-)
%doc
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files compiler
%{_bindir}/thrift


%changelog
